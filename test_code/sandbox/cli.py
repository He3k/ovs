#!/usr/bin/python3
import socket
import threading
import time
import sys

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    FAIL = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def change_matrix(x, y, player):
    if player == 1:
        matrix[y][x] = "[x]"
    elif player == 2:
        matrix[y][x] = "[o]"
    else:
        print(bcolors.FAIL + "Ошибка номера игрока {player}" + bcolors.ENDC)

def is_valid_move(x, y):
    if x == -160 and y == -160:
        return False
    elif x < 0 or x > 2 or y < 0 or y > 2 or not(isinstance(x, int)) or not(isinstance(y, int)) and x.isdigit() and y.isdigit():
        print(bcolors.FAIL + "Ошибка ввода или не соответствует диапазону [1-3]" + bcolors.ENDC)
        return False
    elif matrix[y][x] != "[ ]":
        print("Область занята")
        return False
    return True

def check_coords(message):
    numx = int(message[0])
    numy = int(message[1])
    np = int(message[2])
    if is_valid_move(numx, numy):
        change_matrix(numx, numy, np)
    else:
        print(bcolors.FAIL + "Неверный ввод" + bcolors.ENDC)


def print_matrix():
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)

def receive_messages(sock):
    while True:
        message = sock.recv(1024).decode('utf-8')
        if message:
            print(f"Получено сообщение: {message}")
            if message == "Вы игрок #1":
                print("Игрок 1, введите координаты:")
            elif message == "Вы игрок #2":
                print("")
            elif message == "777":
                print(bcolors.OKGREEN +"Вы победили!" + bcolors.ENDC)
            elif message == "999":
                print(bcolors.FAIL +"Вы проиграли!" + bcolors.ENDC)
            else:
                try: 
                    check_coords(message)
                    print_matrix()
                except ValueError:
                    print("")
        else:
            break

def main(server_addresses):
    for server_address in server_addresses:
        try:
            # Соединение с сервером
            client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            client_socket.connect(server_address)
            print(f"Подключено к серверу {server_address}")
            threading.Thread(target=receive_messages, args=(client_socket,)).start()
            send_messages(client_socket)
        except socket.error as e:
                print(f"{bcolors.FAIL}Не удалось подключиться к серверу {server_address}: {e}{bcolors.ENDC}")
                time.sleep(2) # Пауза перед попыткой подключения к следующему серверу
        except KeyboardInterrupt:
            print(f"{bcolors.FAIL}Прервано пользователем.{bcolors.ENDC}")
            break  # Вместо return используем break для выхода из цикла
        finally:
            client_socket.close()

def send_messages(sock):
    try:
        while True:
            num1 = num2 = -159
            while (not is_valid_move(num1 - 1, num2 - 1)):
                x = input("Введите x >> ")
                y = input("Введите y >> ")
                num1 = int(x)
                num2 = int(y)
            message = f"{num1 - 1}{num2 - 1}"
            sock.send(message.encode('utf-8'))
    except ValueError:
        print(f"{bcolors.FAIL}Неверный ввод!{bcolors.ENDC}")
    except KeyboardInterrupt:
        raise KeyboardInterrupt  # Передаем прерывание чтобы завершить работу клиента

if __name__ == '__main__':
    matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
    server_list = [('127.0.0.1', 5000), ('127.0.0.1', 5001)]  # список серверов
    main(server_list)