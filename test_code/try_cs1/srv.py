# server.py

import socket

def winner(counter):
    if counter == "[x][x][x]":
        return "Win Player 1"
    if counter == "[o][o][o]":
        return "Win Player 0"
    return "No winner yet"

def check_horizontal(matrix, i):
    counter = "".join(matrix[i])
    return winner(counter)

def check_vertical(matrix, i):
    counter = "".join(row[i] for row in matrix)
    return winner(counter)

def check_diagonal(matrix):
    diagonal1 = "".join(matrix[i][i] for i in range(3))
    diagonal2 = "".join(matrix[i][2-i] for i in range(3))
    return winner(diagonal1) or winner(diagonal2)

def check_status(matrix):
    for i in range(3):
        win = check_horizontal(matrix, i)
        if win != "No winner yet":
            return win
        win = check_vertical(matrix, i)
        if win != "No winner yet":
            return win
    return check_diagonal(matrix)

def print_matrix(matrix):
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)

def main():
    matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
    
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((socket.gethostname(), 1234))
    server_socket.listen(2)

    connections = []
    for _ in range(2):
        conn, addr = server_socket.accept()
        connections.append(conn)
        print(f"Connection from {addr} has been established!")

    finish = "No winner yet"
    while finish == "No winner yet":
        for conn in connections:
            print_matrix(matrix)
            move = conn.recv(1024).decode()
            x, y = map(int, move.split())
            x -= 1
            y -= 1
            if "[x]" not in matrix[y][x] and "[o]" not in matrix[y][x]:
                matrix[y][x] = "[x]" if conn is connections[0] else "[o]"
                finish = check_status(matrix)
                for other_conn in connections:
                    other_conn.send(finish.encode())
                if finish != "No winner yet":
                    break

    for conn in connections:
        print_matrix(matrix)
        conn.send(finish.encode())
        conn.close()

    server_socket.close()

if __name__ == "__main__":
    main()
