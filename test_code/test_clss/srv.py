import socket
import threading
import sys
import time

class Server:
    def __init__(self, port, backup_servers=[]):
        self.port = port
        self.backup_servers = backup_servers
        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.bind(('', self.port))
        self.server_socket.listen(5)
        print(f"Server is listening on port {self.port}")
        threading.Thread(target=self.accept_clients).start()

    def accept_clients(self):
        while True:
            conn, addr = self.server_socket.accept()
            print(f"Client connected from {addr}")
            client_thread = threading.Thread(target=self.client_handler, args=(conn,))
            client_thread.start()

    def client_handler(self, conn):
        while True:
            try:
                data = conn.recv(1024).decode('utf-8')
                if not data:
                    print("Client disconnected")
                    break
                print(f"Received data: {data}")
                # Здесь обработка полученных данных от клиента
            except ConnectionResetError:
                print("Client connection reset")
                break
        conn.close()

    def run(self):
        while True:
            if self.is_alive():
                time.sleep(5)  # Проверка доступности сервера через определенные интервалы
            else:
                print(f"Server on port {self.port} is down. Trying to switch to backup server...")
                self.switch_to_backup_server()

    def is_alive(self):
        # Проверка доступности основного сервера
        return True  # Замените на логику проверки доступности сервера

    def switch_to_backup_server(self):
        connected = False
        for server in self.backup_servers:
            server_ip, server_port = server
            try:
                backup_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                backup_socket.connect((server_ip, server_port))
                backup_socket.close()
                self.port = server_port
                self.server_socket.close()
                self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.server_socket.bind(('', self.port))
                self.server_socket.listen(5)
                print(f"Switched to backup server: {server_ip}:{server_port}")
                connected = True
                break
            except:
                continue

        if not connected:
            print("Failed to switch to backup server. Retrying in 30 seconds...")
            time.sleep(30)