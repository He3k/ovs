# server.py
import socket
import threading

def handle_client(client_socket, other_socket):
    while True:
        try:
            # Получение сообщения с двумя числами от клиента
            numbers_message = client_socket.recv(1024)
            if numbers_message:
                nums = numbers_message.decode('utf-8').strip().split()
                # Проверка, что было отправлено ровно два числа
                if len(nums) == 2 and all(num.isdigit() for num in nums):
                    # Пересылаем сообщение с двумя числами другому клиенту
                    other_socket.send(numbers_message)
                else:
                    # Отправляем ошибку клиенту и закрываем соединение
                    client_socket.send(b"Error: Send exactly two numbers separated by a space.")
                    break
            else:
                break
        except ConnectionResetError:
            break
        except:
            client_socket.send(b"Error: An issue occurred. Please resend the numbers.")
            continue
    client_socket.close()

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('127.0.0.1', 12345))
    server_socket.listen(2)

    print("Server is running and waiting for connections...")

    clients = []

    while len(clients) < 2:
        client_socket, addr = server_socket.accept()
        print(f"Client {addr} connected.")
        clients.append(client_socket)
        if len(clients) == 2:
            threading.Thread(target=handle_client, args=(clients[0], clients[1])).start()
            threading.Thread(target=handle_client, args=(clients[1], clients[0])).start()

if __name__ == '__main__':
    main()
