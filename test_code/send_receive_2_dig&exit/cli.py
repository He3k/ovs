# client.py
import socket

def send_receive_pair(client_socket):
    # Пользователь вводит два числа
    num1, num2 = input("Enter two numbers separated by a space: ").split()
    # Отправляем эти числа серверу
    client_socket.send(f"{num1} {num2}".encode('utf-8'))
    # Получаем пару чисел от сервера
    message = client_socket.recv(1024).decode('utf-8')
    if message.startswith("Error"):
        print(message)
    else:
        print(f"Received a pair from another client: {message}")

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('127.0.0.1', 12345))

    try:
        send_receive_pair(client_socket)
    finally:
        client_socket.close()

if __name__ == '__main__':
    main()
