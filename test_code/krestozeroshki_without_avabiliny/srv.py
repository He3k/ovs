import socket
import threading

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    FAIL = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def winner(counter):
    if counter == "[x][x][x]":
        print("Win Player 1")
        return 1
    if counter == "[o][o][o]":
        print ("Win Player 2")
        return 2
    return -1

def check_horizontal0():
    counter = ""
    for element in matrix[0]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal1():
    counter = ""
    for element in matrix[1]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal2():
    counter = ""
    for element in matrix[2]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_vertical0():
    counter = ""
    for row in matrix:
        counter += row[0]
    finish = winner(counter)
    return finish

def check_vertical1():
    counter = ""
    for row in matrix:
        counter += row[1]
    finish = winner(counter)
    return finish

def check_vertical2():
    counter = ""
    for row in matrix:
        counter += row[2]
    finish = winner(counter)
    return finish

def check_diagonal_ltr():
    counter = ""
    counter = matrix[0][0] + matrix[1][1] + matrix[2][2]
    finish = winner(counter)
    return finish

def check_diagonal_rtl():
    counter = ""
    counter = matrix[0][2] + matrix[1][1] + matrix[2][0]
    finish = winner(counter)
    return finish


def check_win():
    check = -1
    check *= check_horizontal0() 
    check *= check_horizontal1() 
    check *= check_horizontal2()
    check *= check_vertical0()
    check *= check_vertical1()
    check *= check_vertical2()
    check *= check_diagonal_ltr()
    check *= check_diagonal_rtl()
    return check

def change_matrix(x, y, player):
    if player == 1:
        matrix[y][x] = "[x]"
    elif player == 2:
        matrix[y][x] = "[o]"
    else:
        print(bcolors.FAIL + "Ошибка номера игрока {player}" + bcolors.ENDC)

def is_valid_move(x, y):
    if x < 0 or x > 2 or y < 0 or y > 2 or not(isinstance(x, int)) or not(isinstance(y, int)) and x.isdigit() and y.isdigit():
        print(bcolors.FAIL + "Ошибка ввода или не соответствует диапазону [1-3]" + bcolors.ENDC)
        return False
    if matrix[y][x] != "[ ]":
        print(bcolors.FAIL + "Область занята" + bcolors.ENDC)
        return False
    return True

def check_coords(message):
    numx = int(message[0])
    numy = int(message[1])
    np = int(message[2])
    if is_valid_move(numx, numy):
        print(bcolors.OKGREEN + "Валидные данные" + bcolors.ENDC)
        change_matrix(numx, numy, np)
    else:
        print(bcolors.FAIL + "Неверный ввод" + bcolors.ENDC)

def print_matrix():
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)

def handle_client(client_socket, client_number, other_socket, turn_event):
    # Сначала отправляем клиенту его номер
    client_socket.send(f"Вы игрок #{client_number}".encode('utf-8'))
    while True:
        is_client_turn = (client_number == 1 and not turn_event.is_set()) or \
                         (client_number == 2 and turn_event.is_set())
                         
        try:
            # Попытка получить сообщение от клиента
            message = client_socket.recv(1024).decode('utf-8')
            is_client_turn = (client_number == 1 and not turn_event.is_set()) or \
                         (client_number == 2 and turn_event.is_set())
            if not message:
                break
            if is_client_turn:
                # Если наступила очередь клиента, обрабатываем сообщение
                print(f"Клиент {client_number} отправил следующее сообщение: {message}")
                ###################
                ### Перепаковка ###
                ###################

                if client_number == 1:
                    other_number = 2
                else: 
                    other_number = 1
                message_with_np = message + str(client_number)
                
                ###############################
                ### Проверка  на валидность ###
                ###############################

                if message_with_np:
                    print(f"Получено сообщение: {message_with_np}")
                    if message_with_np == "Вы игрок #1":
                        print("Игрок 1, введите координаты:")
                    elif message_with_np == "Вы игрок #2":
                        print("")
                    else:
                        try: 
                            check_coords(message_with_np)
                            print_matrix()
                        except ValueError:
                            print("")
                else:
                    break

                ################
                ### Рассылка ###
                ################

                other_socket.send(message_with_np.encode('utf-8'))
                client_socket.send(message_with_np.encode('utf-8'))

                ##############################
                ### Проверка на победителя ###
                ##############################

                finish = check_win()
                if finish != -1:
                    other_socket.send(f"999".encode('utf-8'))
                    client_socket.send(f"777".encode('utf-8'))
                
                #################
                ### Новый ход ###
                #################
                    
                other_socket.send(f"\nИгрок {other_number}, введите координаты: ".encode('utf-8'))
                # Меняем очередь
                turn_event.set() if client_number == 1 else turn_event.clear()

            else:
                # Если не его очередь, просто игнорируем сообщение
                print(f"Клиент {client_number} пытался написать вне очереди.")
                client_socket.send(f"\nИгрок {other_number}, дождитесь своей очереди".encode('utf-8'))
                                
        except ConnectionResetError:
            print(f"Клиент {client_number} неожиданно отключился")
            break
        except Exception as e:
            print(f"Ошибка с клиентом {client_number}: {e}")
            break

    client_socket.close()
    print(f"Клиент {client_number} отключился")

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('127.0.0.1', 12345))
    server_socket.listen(2)

    print("Сервер запущен и ожидает подключения...")

    clients = []
    turn_event = threading.Event() # Флаг очереди

    # Подключаем двух клиентов
    for client_number in range(1, 3):
        client_socket, addr = server_socket.accept()
        clients.append(client_socket)
        print(f"Клиент {client_number} с адресом {addr} подключен.")
        
    # По умолчанию клиент №1 начинает первым
    turn_event.clear()  # 0 или False означает, что клиент 1 может отправлять сообщение

    # Запускаем потоки, передавая объект Event для контроля очереди
    for i in range(2):
        other = clients[1] if i == 0 else clients[0]
        threading.Thread(target=handle_client, args=(clients[i], i+1, other, turn_event)).start()

if __name__ == '__main__':
    matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
    main()