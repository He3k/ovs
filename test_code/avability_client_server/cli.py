#!/usr/bin/python3
# client.py

import socket
import sys
import time

def client(server_ports):
    while True:
        for port in server_ports:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.connect(('localhost', port))
                    while True:
                        try:
                            number = input("Введите число (или 'exit' для выхода): ")
                            if number.lower() == 'exit':
                                return
                            s.sendall(number.encode())
                        except KeyboardInterrupt:
                            print("Клиент был прерван вручную.")
                            return
                        except Exception as e:
                            print(f"Произошла ошибка: {e}")
                            break
            except ConnectionRefusedError:
                print(f"Сервер на порту {port} недоступен. Пытаюсь подключиться к следующему...")
                time.sleep(1) # небольшая пауза перед следующей попыткой подключения
            except Exception as e:
                print(f"Произошла ошибка: {e}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Использование: python client.py порт1 порт2...")
    else:
        client([int(port) for port in sys.argv[1:]])
