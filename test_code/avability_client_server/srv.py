#!/usr/bin/python3
# server.py

import socket
import sys

def start_server(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('localhost', port))
        s.listen()
        print(f"Сервер запущен на порту {port}...")

        while True:
            conn, addr = s.accept()
            print(f"Соединение с {addr}")
            with conn:
                try:
                    while True:
                        data = conn.recv(1024)
                        if not data:
                            break
                        number = int(data.decode())
                        print(f"Получено число: {number}")
                except ConnectionError:
                    print(f"Соединение с {addr} потеряно.")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Использование: python server.py порт")
    else:
        start_server(int(sys.argv[1]))
