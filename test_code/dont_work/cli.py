# client.py
import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((socket.gethostname(), 1234))

while True:
    server_message = client_socket.recv(1024).decode()
    print(server_message)
    
    # Checking for the winner message format
    if "wins" in server_message or "tie" in server_message:
        break

    # Get the move from the user
    move = input("Enter your move (x,y): ")
    client_socket.send(str.encode(move))
