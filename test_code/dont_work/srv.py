import socket
from _thread import *

def winner(counter):
    if counter == "[x][x][x]":
        return "Player 1 wins!"
    if counter == "[o][o][o]":
        return "Player 2 wins!"
    return None

def check_status(matrix):
    # Check rows
    for row in matrix:
        result = winner("".join(row))
        if result:
            return result
    # Check columns
    for col in range(3):
        result = winner("".join(matrix[row][col] for row in range(3)))
        if result:
            return result
    # Check diagonals
    result = winner("".join(matrix[i][i] for i in range(3)))
    if result:
        return result
    result = winner("".join(matrix[i][2 - i] for i in range(3)))
    if result:
        return result
    # Check for a tie
    if all(elem != "[ ]" for row in matrix for elem in row):
        return "It's a tie!"
    return None

def threaded_client(conn, player, gameId, games):
    conn.send(str.encode("Welcome to Tic Tac Toe! You are Player {}n".format(player + 1)))
    while True:
        try:
            data = conn.recv(2048).decode()
            
            if not data:
                break
            
            if data == "get":
                conn.sendall(str.encode("Board:" + str(games[gameId])))
            else:
                x, y = [int(val) for val in data.split(",")]
                if 0 <= x < 3 and 0 <= y < 3:
                    if games[gameId][y][x] == "[ ]":
                        games[gameId][y][x] = "[x]" if player == 0 else "[o]"
                        status = check_status(games[gameId])
                        if status:
                            conn.sendall(str.encode(status))
                        else:
                            conn.sendall(str.encode("move registered"))
                        
        except socket.error as e:
            print(e)
            break
    print("Connection Closed")
    conn.close()

server = "127.0.0.1"
port = 1234

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:
    #s.bind((server, port))
    s.bind((socket.gethostname(), 1234))
except socket.error as e:
    print(str(e))

s.listen(2)
print("Waiting for connections, Server Started")

currentGameId = 0
games = {currentGameId: [["[ ]" for _ in range(3)] for _ in range(3)]}

while True:
    conn, addr = s.accept()
    print("Connected to:", addr)
    if addr not in games:
        if len(games[currentGameId]) == 2:
            currentGameId += 1
            games[currentGameId] = [["[ ]" for _ in range(3)] for _ in range(3)]
        start_new_thread(threaded_client, (conn, len(games[currentGameId]), currentGameId, games))
        games[currentGameId].append(conn)