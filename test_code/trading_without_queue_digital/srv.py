# server.py
import socket
import threading

# Функция для обработки сообщений от клиента
def handle_client(client_socket, other_socket):
    while True:
        try:
            message = client_socket.recv(1024)
            if message:
                # Пересылаем сообщение от одного клиента другому
                other_socket.send(message)
            else:
                break
        except:
            break
    client_socket.close()

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('127.0.0.1', 12345))
    server_socket.listen(2)

    clients = []
    print("Server is running and waiting for connections...")

    while len(clients) < 2:
        client_socket, addr = server_socket.accept()
        clients.append(client_socket)
        print(f"Client {addr} connected.")

    # Запускаем потоки для двух подключенных клиентов
    threading.Thread(target=handle_client, args=(clients[0], clients[1])).start()
    threading.Thread(target=handle_client, args=(clients[1], clients[0])).start()

if __name__ == '__main__':
    main()
