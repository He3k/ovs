# client.py
import socket
import threading

def receive_messages(socket):
    while True:
        try:
            message = socket.recv(1024).decode('utf-8')
            if message:
                print(f"Received: {message}")
            else:
                break
        except:
            break

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('127.0.0.1', 12345))

    threading.Thread(target=receive_messages, args=(client_socket,)).start()

    try:
        while True:
            message = input()
            client_socket.send(message.encode('utf-8'))
    except KeyboardInterrupt:
        print("You have exited the chat.")
    finally:
        client_socket.close()

if __name__ == '__main__':
    main()
