# server.py
import socket
import threading

# Функция для обработки сообщений от клиента
def handle_client(client_socket, client_number):
    while True:
        try:
            message = client_socket.recv(1024)
            if message:
                print(f"Сообщение от клиента {client_number}: {message.decode('utf-8')}")
            else:
                break
        except ConnectionResetError:
            print(f"Клиент {client_number} неожиданно отключился")
            break
        except Exception as e:
            print(f"Ошибка с клиентом {client_number}: {e}")
            break
    client_socket.close()
    print(f"Клиент {client_number} отключился")

def main():
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('127.0.0.1', 12345))
    server_socket.listen(2)

    print("Сервер запущен и ожидает подключения...")

    client_number = 1
    while True:
        client_socket, addr = server_socket.accept()
        print(f"Клиент с адресом {addr} подключился как клиент {client_number}.")
        # Отправляем клиенту информацию о его номере
        client_socket.send(str(client_number).encode('utf-8'))
        
        threading.Thread(target=handle_client, args=(client_socket, client_number)).start()
        
        if client_number == 2:  # Если подключены оба клиента, выходим из цикла
            break
        client_number += 1

if __name__ == '__main__':
    main()
