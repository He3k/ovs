import socket
import threading

def receive_messages(sock):
    while True:
        try:
            message = sock.recv(1024).decode('utf-8')
            if message:
                print(f"Получено сообщение: {message}")
            else:
                break
        except:
            print("Произошла ошибка.")
            break

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect(('127.0.0.1', 12345))
    
    threading.Thread(target=receive_messages, args=(client_socket,)).start()

    try:
        while True:
            try:
                numbers_input = input("Введите два числа, разделённые пробелом: ")
                number1, number2 = numbers_input.split()
                
                # Проверяем, являются ли введённые значения числами
                if not (number1.isdigit() and number2.isdigit()):
                    raise ValueError
              
                message = f"{number1} {number2}"
                client_socket.send(message.encode('utf-8'))
            except ValueError:
                print("Ошибка: Нужно ввести два числа. Попробуйте ещё раз.")
    except KeyboardInterrupt:
        print("Вы вышли из чата.")
    finally:
        client_socket.close()

if __name__ == '__main__':
    main()