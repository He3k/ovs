#!/usr/bin/python3
import socket
import threading
import sys

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    FAIL = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def winner(counter):
    if counter == "[x][x][x]":
        print("Win Player 1")
        return 1
    if counter == "[o][o][o]":
        print ("Win Player 2")
        return 2
    return -1

def check_horizontal0():
    counter = ""
    for element in matrix[0]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal1():
    counter = ""
    for element in matrix[1]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal2():
    counter = ""
    for element in matrix[2]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_vertical0():
    counter = ""
    for row in matrix:
        counter += row[0]
    finish = winner(counter)
    return finish

def check_vertical1():
    counter = ""
    for row in matrix:
        counter += row[1]
    finish = winner(counter)
    return finish

def check_vertical2():
    counter = ""
    for row in matrix:
        counter += row[2]
    finish = winner(counter)
    return finish

def check_diagonal_ltr():
    counter = ""
    counter = matrix[0][0] + matrix[1][1] + matrix[2][2]
    finish = winner(counter)
    return finish

def check_diagonal_rtl():
    counter = ""
    counter = matrix[0][2] + matrix[1][1] + matrix[2][0]
    finish = winner(counter)
    return finish


def check_win():
    check = -1
    check *= check_horizontal0() 
    check *= check_horizontal1() 
    check *= check_horizontal2()
    check *= check_vertical0()
    check *= check_vertical1()
    check *= check_vertical2()
    check *= check_diagonal_ltr()
    check *= check_diagonal_rtl()
    return check

def change_matrix(x, y, player):
    if player == 1:
        matrix[y][x] = "[x]"
    elif player == 2:
        matrix[y][x] = "[o]"
    else:
        print(bcolors.FAIL + "Ошибка номера игрока {player}" + bcolors.ENDC)

def is_valid_move(x, y):
    if x < 0 or x > 2 or y < 0 or y > 2 or not(isinstance(x, int)) or not(isinstance(y, int)) and x.isdigit() and y.isdigit():
        print(bcolors.FAIL + "Ошибка ввода или не соответствует диапазону [1-3]" + bcolors.ENDC)
        return False
    if matrix[y][x] != "[ ]":
        print(bcolors.FAIL + "Область занята" + bcolors.ENDC)
        return False
    return True

def check_coords(message):
    numx = int(message[0])
    numy = int(message[1])
    np = int(message[2])
    if is_valid_move(numx, numy):
        print(bcolors.OKGREEN + "Валидные данные" + bcolors.ENDC)
        change_matrix(numx, numy, np)
    else:
        print(bcolors.FAIL + "Неверный ввод" + bcolors.ENDC)

def print_matrix():
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)

def client_thread(conn, player_number):
    conn.sendall(f"Вы игрок #{player_number}".encode('utf-8'))
    while True:
        try:
            message = conn.recv(1024).decode('utf-8')
            if not message:
                break
            print(message)
            print(player_number)
            message = message + str(player_number)
            print(message)
            if len(message) == 3 and message.isdigit():  # Предполагаем, что сообщение состоит из 3 цифр
                x, y, player = int(message[0]), int(message[1]), int(message[2])    
                if is_valid_move(x, y) and player == player_number:
                    change_matrix(x, y, player_number)
                    if check_win() == -1:
                        conn.sendall("continue".encode('utf-8'))
                    elif check_win() == 1 or check_win() == 2:
                        conn.sendall("777".encode('utf-8'))  # Отправляем код победы
                    else:
                        conn.sendall("999".encode('utf-8'))  # Отправляем код поражения
                else:
                    conn.sendall("Неверный ход".encode('utf-8'))
            else:
                conn.sendall("Некорректный ввод".encode('utf-8'))
        except ConnectionResetError:
            break
    conn.close()

def main(port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', port))
    server_socket.listen(2)
    print(f"{bcolors.OKGREEN}Сервер ожидает подключения на порту {port}{bcolors.ENDC}")

    player_number = 1

    while True:
        conn, addr = server_socket.accept()
        print(f"{bcolors.OKCYAN}Игрок подключился: {addr}{bcolors.ENDC}")
        threading.Thread(target=client_thread, args=(conn, player_number)).start()
        if player_number == 1:
            player_number = 2
        else:
            break  # Прекращаем ожидание после подключения двух игроков

    server_socket.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print(f"{bcolors.FAIL}Необходимо указать порт в качестве аргумента{bcolors.ENDC}")
        sys.exit()
    port = int(sys.argv[1])
    matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
    main(port)