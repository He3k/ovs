#!/usr/bin/python3

def winner(counter):
    if counter == "[x][x][x]":
        print("Win Player 1")
        return 0
    if counter == "[o][o][o]":
        print ("Win Player 0")
        return 0
    return -1


def check_horizontal0():
    counter = ""
    for element in matrix[0]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal1():
    counter = ""
    for element in matrix[1]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal2():
    counter = ""
    for element in matrix[2]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_vertical0():
    counter = ""
    for row in matrix:
        counter += row[0]
    finish = winner(counter)
    return finish

def check_vertical1():
    counter = ""
    for row in matrix:
        counter += row[1]
    finish = winner(counter)
    return finish

def check_vertical2():
    counter = ""
    for row in matrix:
        counter += row[2]
    finish = winner(counter)
    return finish

def check_diagonal_ltr():
    counter = ""
    counter = matrix[0][0] + matrix[1][1] + matrix[2][2]
    finish = winner(counter)
    return finish

def check_diagonal_rtl():
    counter = ""
    counter = matrix[0][2] + matrix[1][1] + matrix[2][0]
    finish = winner(counter)
    return finish


def check_win():
    check = -1
    check *= check_horizontal0() 
    check *= check_horizontal1() 
    check *= check_horizontal2()
    check *= check_vertical0()
    check *= check_vertical1()
    check *= check_vertical2()
    check *= check_diagonal_ltr()
    check *= check_diagonal_rtl()
    return check
    

def print_matrix():
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)


matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
finish = -1
while(finish):
    print_matrix()
    print("Player 1: \n")
    x = int(input("Enter x >> "))
    x-=1
    y = int(input("Enter y >> "))
    y-=1
    matrix[y][x] = "[x]"
    finish = check_win()
    if(finish):
        print_matrix()
        print("Player 2: \n")
        x = int(input("Enter x >> "))
        x-=1
        y = int(input("Enter y >> "))
        y-=1
        matrix[y][x] = "[o]"
        finish = check_win()
print_matrix()
