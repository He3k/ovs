#!/usr/bin/python3

import socket

def main():
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((socket.gethostname(), 1234))

    finish = "No winner yet"
    while finish == "No winner yet":
        x, y = map(int, input("Enter x y >> ").split())
        client_socket.send(f"{x} {y}".encode())
        finish = client_socket.recv(1024).decode()
        print(finish)

    client_socket.close()

if __name__ == "__main__":
    main()
