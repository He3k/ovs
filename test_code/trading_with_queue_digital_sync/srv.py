import socket
import threading

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(('127.0.0.1', 12345))
server_socket.listen()
print("Сервер начал прослушивание...")

clients = {}
clients_lock = threading.Lock()

def handle_client(client_socket, client_id):
    while True:
        data = client_socket.recv(1024).decode('utf-8')
        
        other_client_id = 1 if client_id == 2 else 2
        other_client_socket = clients[other_client_id]
        
        other_client_socket.send(data.encode('utf-8'))

        response = other_client_socket.recv(1024).decode('utf-8')
        client_socket.send(response.encode('utf-8'))

def accept_clients():
    client_id = 1
    while True:
        client_socket, client_address = server_socket.accept()
        with clients_lock:
            clients[client_id] = client_socket
        client_thread = threading.Thread(target=handle_client, args=(client_socket, client_id))
        client_thread .start()
        client_id += 1
        if client_id > 2:
            break

accept_clients()
