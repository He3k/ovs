import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect(('127.0.0.1', 12345))
print("Подключенеи к серверу установлено")

while True:
    data = input("Введите два числа через пробел: ")
    client_socket.send(data.encode('utf-8'))
    
    response = client_socket.recv(1024).decode('utf-8')
    print(f"Получено от другого клиента: {response}")
