#!/usr/bin/python3
import socket
import threading
import sys

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    FAIL = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def winner(counter):
    if counter == "[x][x][x]":
        print("Win Player 1")
        return 1
    if counter == "[o][o][o]":
        print ("Win Player 2")
        return 2
    return -1

def check_horizontal0():
    counter = ""
    for element in matrix[0]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal1():
    counter = ""
    for element in matrix[1]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_horizontal2():
    counter = ""
    for element in matrix[2]:
        counter = counter + element
    finish = winner(counter)
    return finish

def check_vertical0():
    counter = ""
    for row in matrix:
        counter += row[0]
    finish = winner(counter)
    return finish

def check_vertical1():
    counter = ""
    for row in matrix:
        counter += row[1]
    finish = winner(counter)
    return finish

def check_vertical2():
    counter = ""
    for row in matrix:
        counter += row[2]
    finish = winner(counter)
    return finish

def check_diagonal_ltr():
    counter = ""
    counter = matrix[0][0] + matrix[1][1] + matrix[2][2]
    finish = winner(counter)
    return finish

def check_diagonal_rtl():
    counter = ""
    counter = matrix[0][2] + matrix[1][1] + matrix[2][0]
    finish = winner(counter)
    return finish


def check_win():
    check = -1
    check *= check_horizontal0() 
    check *= check_horizontal1() 
    check *= check_horizontal2()
    check *= check_vertical0()
    check *= check_vertical1()
    check *= check_vertical2()
    check *= check_diagonal_ltr()
    check *= check_diagonal_rtl()
    return check

def change_matrix(x, y, player):
    if player == 1:
        matrix[y][x] = "[x]"
    elif player == 2:
        matrix[y][x] = "[o]"
    else:
        print(bcolors.FAIL + "Ошибка номера игрока {player}" + bcolors.ENDC)

def is_valid_move(x, y):
    if x < 0 or x > 2 or y < 0 or y > 2 or not(isinstance(x, int)) or not(isinstance(y, int)) and x.isdigit() and y.isdigit():
        print(bcolors.FAIL + "Ошибка ввода или не соответствует диапазону [1-3]" + bcolors.ENDC)
        return False
    if matrix[y][x] != "[ ]":
        print(bcolors.FAIL + "Область занята" + bcolors.ENDC)
        return False
    return True

def check_coords(message):
    numx = int(message[0])
    numy = int(message[1])
    np = int(message[2])
    if is_valid_move(numx, numy):
        print(bcolors.OKGREEN + "Валидные данные" + bcolors.ENDC)
        change_matrix(numx, numy, np)
    else:
        print(bcolors.FAIL + "Неверный ввод" + bcolors.ENDC)

def print_matrix():
    for row in matrix:
        row_str = ' '.join([str(element) for element in row])
        print(row_str)

def send_to_clients(message, clients):
   # Send the message to both clients
    try:
        for client in clients:
            client.sendall(message.encode('utf-8'))
    except Exception as e:
        print(f"Error sending to other server: {e}")
 

def handle_client(conn, player_number, clients, other_server_ip, other_server_port):
    conn.sendall(f"You are player #{player_number}".encode('utf-8'))
    while True:
        try:
            message = conn.recv(1024).decode('utf-8')
            if not message:
                break
            print(message)
            print(len(message))
            if (len(message) > 2):
                clients.clear()
            try: 
                check_coords(message+str(player_number))
                print_matrix()
            except ValueError:
                 print("Incorrect coords")
            send_to_other_server(message, other_server_ip, other_server_port, player_number)
            message = message + str(player_number)
            send_to_clients(message, clients)
            finish = check_win()
            if finish != -1:
                    if(player_number == 1):
                        clients[0].send(f"777".encode('utf-8'))
                        clients[1].send(f"999".encode('utf-8'))
                    else:
                        clients[0].send(f"999".encode('utf-8'))
                        clients[1].send(f"777".encode('utf-8'))
                
        except ConnectionResetError:
            print("Client connection reset.")
            break
    conn.close()

def send_to_other_server(message, other_server_ip, other_server_port, pn):
    try:
        message = message + str(pn)
        other_server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        other_server_socket.connect((other_server_ip, other_server_port))
        other_server_socket.send(message.encode('utf-8'))
        other_server_socket.close()
    except Exception as e:
        print(f"Error sending to other server: {e}")

def main(current_port, other_server_ip, other_server_port):
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(('', current_port))
    server_socket.listen(2)
    print(f"{bcolors.OKGREEN}Server is running and waiting for connections on port {current_port}{bcolors.ENDC}")
    while True:
        conn, addr = server_socket.accept()
        clients.append(conn)
        print(f"{bcolors.OKCYAN}Client connected: {addr}{bcolors.ENDC}")
        threading.Thread(target=handle_client, args=(conn, len(clients), clients, other_server_ip, other_server_port)).start()

if __name__ == '__main__':
    clients = []
    matrix = [["[ ]" for _ in range(3)] for _ in range(3)]
    if len(sys.argv) < 4:
        print(f"{bcolors.FAIL}Arguments missing. Usage: python server.py [current_port] [other_server_ip] [other_server_port]{bcolors.ENDC}")
        sys.exit()

    current_port = int(sys.argv[1])
    other_server_ip = sys.argv[2]
    other_server_port = int(sys.argv[3])

    threading.Thread(target=main, args=(current_port, other_server_ip, other_server_port)).start()